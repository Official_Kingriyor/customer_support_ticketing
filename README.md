Customer Support API
==============================================


API setup and execution
----------------------------
### Dependencies
- node      (version - v14.5.0)
- mongodb   (version - 4.2.0)
- 

### To run

- install node modules
```
npm install
```

- create a file named '.env' in the same dir as 'sample.env'

- populate it with environment variables (sample environment variables shown below)

- run database setup (To create db, and create default agent, admin and customer accounts) NB to get credentials of sample account check 'app/dbsetup.js'
```
npm run dbsetup
```

- run test
```
npm run test
```

- run api
```
npm run api
```


## Routes
!important - All routes must contain a Authentication header except /users/register  (get credentials from 'app/dbsetup.js')

Allowed Access     			| END POINT                  | Description
--------------------------- | -------------------------- | ------------------------------------------------------------
NO Auth                     | /                          | GET      Base Endpoint / Welcome endpoint
'admin'                     | /users                     | POST     Create a user
'admin','agent'             | /users                     | GET      Get all users 
'admin','agent','customer'  | /users/email               | PUT      Update user by email
'admin','agent','customer'  | /users/email               | GET      Get user by email
'admin'                     | /users/email               | DELETE   Delete user by email
NO Auth                     | /users/register            | POST     Register as a new user(customer)
NO Auth                     | /login                     | POST     login to get token
'admin'                     | /users/deactivate/email    | GET      deactivate a user by email
'admin'                     | /users/reactivate/email    | GET      reactivate a user by email
                            |
'customer'                  | /tickets                   | POST     Open a ticket 
'admin','agent','customer'  | /tickets                   | GET      Get all tickets
'admin','agent','customer'  | /tickets/:ticket_id        | GET      Get ticket by ticket_id
'admin'                     | /tickets/:ticket_id        | DELETE   Delete ticket by ticket_id
'admin','agent','customer'  | /tickets/comment           | POST     Add a comment
'admin','agent'             | /tickets/export            | GET      Download csv file report of tickets closed in the last month
'admin','agent'             | /tickets/process           | POST     Start processing ticket
'admin','agent'             | /tickets/close             | POST     Close a ticket
                                


## Design docs
- found in 'design_docs' folder

**Sample Requests and Responses can be found in the postman collection**
- import "Fliqpay - Customer Support.postman_collection.json" into your postman collection


**Environment Variables:**
```
API_PORT=3000
NODE_PATH='./app'
MONGO_HOST=127.0.0.1
MONGO_USER=
MONGO_PASSWORD=
MONGO_PORT=27017
MONGO_DB_NAME='customer_support_ticketing'
TIMEOUT=120000

ENABLE_FILE_LOGGING=false
APP_LOG_PATH=../

TOKEN_SECRET='app_secret'
```

## Data Dictionary

### ticketSchema

field     			            | unique          | compulsary          | description
--------------------------- | --------------- | ------------------- | ----------------------------------------
  ticket_id                 | unique:true     | compulsary:true     | auto-generated id unique to a ticket
  ticket_description        | unique:false    | compulsary:true     | this describes the issue the ticket is to address
  estimated_duration_days   | unique:false    | compulsary:false    | added when agent starts to process a ticket. it describes the estimated time in days to close the ticket
  agent_email               | unique:false    | compulsary:false    | email of agent processing ticket
  customer_email            | unique:false    | compulsary:true     | email of customer who raised the ticket
  request_date              | unique:false    | compulsary:true     | date ticket was rasied
  process_start_date        | unique:false    | compulsary:false    | date agent started processing ticket 
  process_end_date          | unique:false    | compulsary:false    | date agent finished processing ticket (closed the ticket)
  ticket_status             | unique:false    | compulsary:true     | shows current status of ticket
  allow_comments            | unique:false    | compulsary:true     | this is set to true if agent or admin has commented first on a ticket therefore customer can now comment 
  comments                  | unique:false    | compulsary:false    | an array holding all comments of current ticket

### userSchema

field     			            | unique          | compulsary          | description
--------------------------- | --------------- | ------------------- | ----------------------------------------
  email                     | unique:true     | compulsary:true     | user's email
  name                      | unique:false    | compulsary:true     | user's name
  password                  | unique:false    | compulsary:true     | user's password (encrypted)
  privilege                 | unique:false    | compulsary:true     | user's priviledge (admin,agent,customer)
  user_status               | unique:false    | compulsary:true     | user's status (active,inactive)