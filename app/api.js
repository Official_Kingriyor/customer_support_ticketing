/**
 * Created by Kingriyor
 */

const dotenv = require('dotenv');

dotenv.config();

const express = require('express');
const app = express();

app.set('trust proxy', true);
const cookieParser = require('cookie-parser');
app.use(cookieParser());
const server = express.Router();

const config = require('./config/settings');
const ticketRoutes = require('./routes/ticket');
const userRoutes = require('./routes/user');


// service locator via dependency injection
const serviceLocator = require('./config/di');

const logger = serviceLocator.get('logger');

// Connect to Mongo
serviceLocator.get('mongo');

// setup Routing and Error Event Handling
ticketRoutes(server, serviceLocator);
userRoutes(server, serviceLocator);


app.use('/', server);

app.listen(config.api_server.port, function() {
    console.info('customer_support_ticketing running on port %s', config.api_server.port);
});

module.exports = app;
