/* eslint-disable no-unused-vars */
/**
 * Created by Kingriyor
 */
const bluebird = require('bluebird');
const mongoose = require('mongoose');

mongoose.Promise = bluebird;
const config = require('../config/settings');
const serviceLocator = require('../lib/service_locator');

const TicketService = require('../services/ticket');
const TicketController = require('../controllers/ticket');

const UserController = require('../controllers/user');
const UserService = require('../services/user');

const winston = require('winston');
require('winston-daily-rotate-file');

/**
 * Returns an instance of logger
 */
serviceLocator.register('logger', () => {
  const fileTransport = new (winston.transports.DailyRotateFile)({
    filename: `${config.logging.file}se_app.log`,
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: process.env.ENV === 'development' ? 'debug' : 'info',
  });

  const consoleTransport = new (winston.transports.Console)({
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    json: false,
    colorize: true,
    level: process.env.ENV === 'development' ? 'debug' : 'info',
  });
  const transports = [consoleTransport];
  const winstonLogger = new (winston.Logger)({
    transports,
  });
  return winstonLogger;
});


/**
 * Returns a Mongo connection instance.
 */
serviceLocator.register('mongo', (servicelocator) => {
  const logger = servicelocator.get('logger');
  const connectionString = (!config.mongodb.username || !config.mongodb.password) ? `mongodb://${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.db}` : `mongodb://${config.mongodb.username}:${config.mongodb.password}@${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.db}`;
  mongoose.Promise = bluebird;
  const mongo = mongoose.connect(connectionString);
  mongo.connection.on('connected', () => {
    logger.info('Mongo Connection Established');
  });
  mongo.connection.on('error', (err) => {
    logger.error(`Mongo Connection Error : ${err}`);
    process.exit(1);
  });
  mongo.connection.on('disconnected', () => {
    logger.error('Mongo Connection disconnected');
    process.exit(1);
  });

  // If the Node process ends, close the Mongoose connection
  process.on('SIGINT', () => {
    mongo.connection.close(() => {
      logger.error('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
  });

  return mongo;
});

/**
 * Creates an instance of the Ticket Service
 */
serviceLocator.register('ticketService', (servicelocator) => {
  const logger = servicelocator.get('logger');
  const mongoclient = servicelocator.get('mongo');
  return new TicketService(logger, mongoclient);
});


/**
 * Creates an instance of the Ticket controller
 */
serviceLocator.register('ticketController', (servicelocator) => {
  const logger = servicelocator.get('logger');
  const ticketService = servicelocator.get('ticketService');

  return new TicketController(logger, ticketService);
});

/**
 * Creates an instance of the User Service
 */
serviceLocator.register('userService', (servicelocator) => {
  const logger = servicelocator.get('logger');
  const mongoclient = servicelocator.get('mongo');
  return new UserService(logger, mongoclient);
});


/**
 * Creates an instance of the User controller
 */
serviceLocator.register('userController', (servicelocator) => {
  const logger = servicelocator.get('logger');
  const userService = servicelocator.get('userService');

  return new UserController(logger, userService);
});


module.exports = serviceLocator;
