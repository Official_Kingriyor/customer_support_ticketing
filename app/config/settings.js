/* eslint-disable no-template-curly-in-string */
/**
 * Created by Kingriyor
 */

const config = {
  delimiter: ':\n',
  api_server: {
    port: process.env.API_PORT,
  },
  auth: {
    token: process.env.TOKEN_SECRET
  },
  logging: {
    shouldLogToFile: process.env.ENABLE_FILE_LOGGING,
    file: process.env.LOG_PATH,
    level: process.env.LOG_LEVEL || 'warn',
    console: process.env.LOG_ENABLE_CONSOLE || true,
  },
  mongodb: {
    host: process.env.MONGO_HOST,
    username: process.env.MONGO_USER,
    password: process.env.MONGO_PASSWORD,
    port: process.env.MONGO_PORT,
    db: process.env.MONGO_DB_NAME,
    collections: {
      tickets: 'tickets',
      user: 'users',

    },
    query_limit: process.env.MONGODB_QUERY_LIMIT,
  },
  ticket_status: {
    pending: 'pending',
    processing: 'processing',
    blocked: 'blocked',
    closed: 'closed'
  },
  mongo_error_code: {
    duplicate_id: 11000
  },
  app_name: 'customer_support'
};

module.exports = config;
