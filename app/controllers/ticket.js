
/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
/* eslint-disable consistent-return */
/* eslint-disable camelcase */
/**
 * Created by Kingriyor
 */

// imported modules
const Response = require('../lib/response_manager');
const HttpStatus = require('../constants/http_status');
var short_uuid = require('short-uuid');
const config = require('../config/settings');
const AuthHelper = require('../lib/auth_helper');
const util = require('../lib/util');

class Ticket {
  /**
   * Class Constructor
   *
   * @param logger - winston logger
   * @param ticketService 
   */
  constructor(logger, ticketService) {
    this.logger = logger;
    this.ticketService = ticketService;
  }

  /**
   * Gets Ticket information by ticket_id
   *
   * @param req
   * @param res
   * @methodVerb GET
   */

  async getByTicketById(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent','customer']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    const { ticket_id } = req.params;
    const params = {};
    

    params.user_privilege = user_data.privilege
    // ensures a customer can only get their own records
    if (user_data.privilege === 'customer'){
      params.customer_email = user_data.email
    }
    params.ticket_id = ticket_id

    return this.ticketService
      .getAllTickets(params)
      .then((response) => {
        // if (!count[0] && !Number.isInteger(count)) count = 0;
        // console.log(response)
        if(response.length === 0){
          Response.failure(
            res,
            {
              message: "Ticket request not found",
              response: {}
            },
            HttpStatus.NOT_FOUND
          );
        }
        Response.success(res, {
          message: `${response.length} tickets were successfully fetched`,
          response: response[0]
        });
      })
      .catch((error) => {
        this.logger.error('err1', error);
        Response.failure(
          res,
          {
            message: error.msg,
            response: {}
          },
          HttpStatus.NOT_FOUND
        );
      })
  }

  // get the bulk data with the total Tickets
  async getTickets(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent','customer']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    const params = req.query;
    
    params.user_privilege = user_data.privilege
    // ensures a customer can only get their own records
    if (user_data.privilege === "customer"){
      params.customer_email = user_data.email
    }

    return this.ticketService
      .getAllTickets(params)
      .then((response) => {
        // if (!count[0] && !Number.isInteger(count)) count = 0;
        Response.success(res, {
          message: `${response.length} tickets were successfully fetched`,
          response: response
        });
      })
      .catch((error) => {
        this.logger.error('err1', error);
        Response.failure(
          res,
          {
            message: error.msg,
            response: {}
          },
          HttpStatus.NOT_FOUND
        );
      })
  }

  async getPastMonth(){
    var d = new Date();
    var m = d.getMonth();
    d.setMonth(d.getMonth() - 1);

    // If still in same month, set date to last day of 
    // previous month
    if (d.getMonth() == m) d.setDate(0);
    d.setHours(0, 0, 0);
    d.setMilliseconds(0);

    let dateString = d.toISOString().slice(0,10)

    // string to date object
    let dateObj = new Date(dateString)  

    //date object to epoch
    let epochDate = dateObj.getTime() / 1000

    return epochDate
  }

  /**
   *
   * @param req
   * @param res
   * @methodVerb POST
   */
  async createTicket(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['customer']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    const ticket_id = short_uuid.generate()
    // check if any of the required parameters were not sent
    if (!req.body.ticket_description) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields - ticket_description'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const { ticket_description } = req.body;
  

    const ticketData = {
      ticket_description,
      request_date: Date(),
      ticket_id
    };

    const { ticketService } = this;

    // console.log(user_data)
    
    ticketData.customer_email = user_data.email;

    ticketData.ticket_status = config.ticket_status.pending;
    // blancks in create include below
    // process_start_date, process_end_date, allow_comments, comments
    

    return ticketService
      .createTicket(ticketData)
      .then((result) => {
        this.logger.info('Created Ticket successfully', result);
        if (result.errors){
          Response.failure(res, {
            message: result.errors,
            response: {},
          }, HttpStatus.BAD_REQUEST);
        }
        return Response.success(res,{
            message: 'Ticket was successfully created',
            response: {ticket_id}
          },
          HttpStatus.OK
        );
      })
      .catch((createTicketError) => {
        this.logger.error('Unable to create Ticket ======>', createTicketError);
        return Response.failure(
          res,
          {
            message: createTicketError.msg,
            response: {}
          },
          HttpStatus.BAD_REQUEST
        );
      });
       
  }

  async addComment(req, res) {
    
    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent','customer']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------


    // check if any of the required parameters were not sent
    if (!req.body.comment || !req.body.ticket_id) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields - comment and ticket_id '
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const {
      comment,
      ticket_id
    } = req.body;

    const { ticketService } = this;
    let ticketData = {}
    ticketData.comment = comment;

    ticketData.user_email = user_data.email;
    const user_privilege = user_data.privilege
    
    return ticketService
      .addComment(user_privilege, ticket_id, ticketData)
      .then((result) => {
        this.logger.info('Comment successful', result);
        if (result.error){
          return Response.failure(
            res,
            {
              message: result.data,
              response: {}
            },
            HttpStatus.BAD_REQUEST
          );
        }
        return Response.success(
          res,
          {
            message: 'Comment successfully added',
            response: {}
          },
          HttpStatus.OK
        );
      })
      .catch((createTicketError) => {
        this.logger.error('Unable to create Comment ===>', createTicketError);
        return Response.failure(
          res,
          {
            message: createTicketError.msg,
            response: {}
          },
          HttpStatus.BAD_REQUEST
        );
      });
       
  }

  async processTicket(req, res){
    // ensure estimated_duration_days is in days

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    // check if any of the required parameters were not sent
    if (!req.body.ticket_id || !req.body.estimated_duration_days) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields - ticket_id and estimated_duration_days'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    if(isNaN(req.body.estimated_duration_days)){
      return Response.failure(
        res,
        {
          message:
            'estimated_duration_days must be a valid number'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const {
      estimated_duration_days,
      ticket_id
    } = req.body;

    const process_start_date = Math.floor(Date.now() / 1000)
    const agent_email = user_data.email
    const ticket_status = config.ticket_status.processing
    const user_privilege = user_data.privilege

    const { ticketService } = this;
    let ticketData = {
      ticket_status,
      agent_email,
      process_start_date,
      estimated_duration_days
    }

    return ticketService
      .updateTicket(user_privilege, ticket_id, ticketData)
      .then((result) => {
        this.logger.info('Ticket update successful', result);
        if (result.error){
          Response.failure(res, {
            message: result.data,
            response: {},
          }, HttpStatus.BAD_REQUEST);
        }
        return Response.success(res,{
            message: 'Ticket was successfully updated',
            response: {}
          },
          HttpStatus.OK
        );
      })
      .catch((updateTicketError) => {
        this.logger.error('Unable to update Ticket ======>', updateTicketError);
        return Response.failure(
          res,
          {
            message: updateTicketError.msg,
            response: {}
          },
          HttpStatus.BAD_REQUEST
        );
      });



  }

  async closeTicket(req, res){

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    // check if any of the required parameters were not sent
    if (!req.body.ticket_id) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields - ticket_id'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const {
      ticket_id
    } = req.body;

    const process_end_date = Math.floor(Date.now() / 1000)
    const agent_email = user_data.email
    const ticket_status = config.ticket_status.closed
    const user_privilege = user_data.privilege

    const { ticketService } = this;
    let ticketData = {
      ticket_status,
      agent_email,
      process_end_date
    }

    return ticketService
      .updateTicket(user_privilege, ticket_id, ticketData)
      .then((result) => {
        this.logger.info('Ticket update successful', result);
        if (result.error){
          Response.failure(res, {
            message: result.data,
            response: {},
          }, HttpStatus.BAD_REQUEST);
        }
        return Response.success(res,{
            message: 'Ticket was successfully updated',
            response: {}
          },
          HttpStatus.OK
        );
      })
      .catch((updateTicketError) => {
        this.logger.error('Unable to update Ticket ======>', updateTicketError);
        return Response.failure(
          res,
          {
            message: updateTicketError.msg,
            response: {}
          },
          HttpStatus.BAD_REQUEST
        );
      });
  }

  async exportClosedTickets(req, res){
    
    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    const params = req.query;
    const pastMonth = await this.getPastMonth();
    const currentEpoch = Math.floor(Date.now() / 1000);

    const filename = 'report.csv'
    
    params.user_privilege = user_data.privilege
    // ensures a customer can only get their own records
    if (user_data.privilege === "customer"){
      params.customer_email = user_data.email
    }

    params.past_month = pastMonth
    params.current_epoch = currentEpoch
    params.ticket_status = 'closed'


    return this.ticketService
      .getAllTickets(params)
      .then((response) => {
        return util.downloadResource(res, filename, response )
      })
      .catch((error) => {
        this.logger.error('err1', error);
        Response.failure(
          res,
          {
            message: error.msg,
            response: {}
          },
          HttpStatus.NOT_FOUND
        );
      })

  }

  async deleteTicket(req, res){
    // TODO
    // This is to be used for test purposes only. A ticket isn't meant to be deleted

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    // check if any of the required parameters were not sent
    if (!req.params.ticket_id) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all param - ticket_id'
        },
        HttpStatus.BAD_REQUEST
      );
    }
    const { ticketService } = this;
    const { ticket_id } = req.params;
    

    return ticketService
      .deleteTicket(ticket_id)
      .then((result) => {
        // this.logger.info('Deleted Ticket successfully', result);
        if (!result || result.errors || result.error){
          Response.failure(res, {
            message: result.errors,
            response: result.data,
          }, HttpStatus.BAD_REQUEST);
        }
        return Response.success(res,{
            message: 'Ticket was successfully deleted',
            response: {ticket_id}
          },
          HttpStatus.OK
        );
      })
      .catch((deleteTicketError) => {
        this.logger.error('Unable to delete Ticket ======>', deleteTicketError);
        return Response.failure(
          res,
          {
            message: createTicketError.msg,
            response: {}
          },
          HttpStatus.BAD_REQUEST
        );
      });
  }

}

module.exports = Ticket;
