/* eslint-disable prefer-const */
/* eslint-disable radix */
/* eslint-disable camelcase */
/**
 * Created by Kingriyor
 *
 */

// imported modules
const Response = require('../lib/response_manager');
const HttpStatus = require('../constants/http_status');
const md5 = require('md5')
const AuthHelper = require('../lib/auth_helper');


class UserController {
  /**
      * Class Constructor
      *
      * @param logger - winston logger
      * @param userService
      */
  constructor(logger, userService) {
    this.logger = logger;
    this.userService = userService;
  }

  handleError(error, res) {
    // this.logger.info(error);
    if (error.name === 'DocumentNotFoundError') {
      return Response.failure(res, {
        message: error.message,
        response: {},
      }, HttpStatus.NOT_FOUND);
    } else {
      return Response.failure(res, {
        message: error,
        response: {},
      }, HttpStatus.BAD_REQUEST);
    }
  }

  async login(req, res){
    if (!req.body.email || !req.body.password) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields email, password'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    let { email, password } = req.body;
    password = md5(password)

    try {
      const user = await this.userService.getUserByEmailAndPassword(email,password);
      // console.log(user._doc)
      if (user == null){
        return Response.failure(
          res,
          {
            message:
              'email and password not found'
          },
          HttpStatus.BAD_REQUEST
        );
      }

      if (user._doc.user_status == 'inactive'){
        return Response.failure(
          res,
          {
            message:
              'account has been deactivated'
          },
          HttpStatus.BAD_REQUEST
        );
      }
      


      const privilege = user._doc.privilege
  
      const token = AuthHelper.generateAccessToken(email, privilege);
      
      Response.success(res, {
        message: 'login successful',
        response: {
          token : token
        }
      });
    } catch (e) {
      this.handleError(e, res);
    }
  }

  /**
      * Gets user information by user_id
      *
      * @param req
      * @param res
      * @methodVerb GET
      */

  async getUserByEmail(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent','customer']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    const auth_email = user_data.email;
    const auth_privilege = user_data.privilege;
    

    if (!req.query.email) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all field email to params'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const { email } = req.query;
    try {
      // console.log(user_data)
      // If u customer made a call to this endpoint then he/she is to get their info instead
      // ensures a customer can only get their own records
      if(auth_privilege === "customer"){
        const user = await this.userService.getUserByEmail(auth_email);

        if (!user){
          return Response.failure(
            res,
            {
              message:
                'User not found'
            },
            HttpStatus.NOT_FOUND
          );
        }
        Response.success(res, {
          message: 'user fetched',
          response: user
        });
      }else{
        const user = await this.userService.getUserByEmail(email);
        if (!user){
          return Response.failure(
            res,
            {
              message:
                'User not found'
            },
            HttpStatus.NOT_FOUND
          );
        }
        Response.success(res, {
          message: 'user fetched',
          response: user
        });
      }

      
    } catch (e) {
      this.handleError(e, res);
    }
  }

  async getAllUsers(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------


    // const { size, page, email, privilege, name } = req.query;

    try {
      const users = await this.userService.getAllUsers(req.query);

      Response.success(res, {
        message: `${users.length} users were successfully fetched`,
        response: {
          response: users,
          total: users.length
        }
      });
    } catch (e) {
      this.handleError(e, res);
    }
  }

  async createUser(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    if (!req.body.name || !req.body.email || !req.body.password || !req.body.privilege) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields name , email, password, privilege '
        },
        HttpStatus.BAD_REQUEST
      );
    }

    
    let {
      name, email, password, privilege
    } = req.body;

    password = md5(password)

    const allowed_privileges = ['admin','agent','customer']
    if(allowed_privileges.indexOf(privilege) === -1){
      Response.failure(res, {
        message: "permitted privileges includes admin, agent and customer",
        response: {},
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    const userData = {
      // user_id: short_uuid.generate(),
      name,
      email,
      password,
      privilege,
      user_status : 'active'
    };



    try {
      const create_user_res = await this.userService.createUser(userData);
      // console.log(create_user_res.errmsg)
      if (create_user_res.errmsg){
        Response.failure(res, {
          message: create_user_res.errmsg,
          response: {},
        }, HttpStatus.FORBIDDEN);
      }

      Response.success(res, {
        message: 'User has been created successfully',
        response: {email}
      });
    } catch (e) {
      Response.failure(res, {
        message: e.message,
        response: {},
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async deactivareUser(req, res){
    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    if (!req.query.email) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all param - email'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const { email } = req.query;

    const userData = {
      email,
      user_status: "inactive"
    };

    try {
      const result = await this.userService.updateUserStatus(userData);
      Response.success(res, {
        message: 'user deactivated successfully',
        response: {email}
      });
    
    } catch (e) {
      this.logger.info('err', e);
      Response.failure(res, {
        message: e.message,
        response: {},
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }


  }

  async reactivareUser(req, res){
    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    if (!req.query.email) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all param - email'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const { email } = req.query;

    const userData = {
      email,
      user_status: "active"
    };

    try {
      const result = await this.userService.updateUserStatus(userData);
      Response.success(res, {
        message: 'user reactivated successfully',
        response: {email}
      });
    
    } catch (e) {
      this.logger.info('err', e);
      Response.failure(res, {
        message: e.message,
        response: {},
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }


  }

  async registerNewCustomer(req, res) {

    if (!req.body.name || !req.body.email || !req.body.password) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields name , email, password '
        },
        HttpStatus.BAD_REQUEST
      );
    }

    
    let {
      name, email, password
    } = req.body;

    password = md5(password)
    const privilege = 'customer'

    // const allowed_privileges = ['admin','agent','customer']
    // if(allowed_privileges.indexOf(privilege) === -1){
    //   Response.failure(res, {
    //     message: "permitted privileges includes admin, agent and customer",
    //     response: {},
    //   }, HttpStatus.INTERNAL_SERVER_ERROR);
    // }

    const userData = {
      // user_id: short_uuid.generate(),
      name,
      email,
      password,
      privilege,
      user_status : 'active'
    };

    try {
      const create_user_res = await this.userService.createUser(userData);
      if (create_user_res.errmsg){
        Response.failure(res, {
          message: create_user_res.errmsg,
          response: {},
        }, HttpStatus.FORBIDDEN);
      }

      Response.success(res, {
        message: 'User has been created successfully',
        response: {}
      });
    } catch (e) {
      Response.failure(res, {
        message: e.message,
        response: {},
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async updateUserByEmail(req, res) {

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin','agent','customer']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    const auth_email = user_data.email;
    const auth_privilege = user_data.privilege;
    

    // the only field that can be updated in user is the user name

    if (!req.body.name || !req.body.email) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all fields - email , name'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const { name, email } = req.body;

    const userData = {
      email,
      name
    };

    try {
      // console.log(user_data)
      // If u customer made a call to this endpoint then he/she is to get their info instead
      // ensures a customer can only get their own records
      if(auth_privilege === "customer"){

        const user = await this.userService.updateUserName(auth_email, name);
        if (user.error){
          return Response.failure(res, {
              message: user.data
            },
            HttpStatus.BAD_REQUEST
          );

        }else{
          Response.success(res, {
            message: 'user updated',
            response: {
              email
            }
          });
        }
        
      }else{

        const user = await this.userService.updateUserName(email, name);
        if (user.error){
          return Response.failure(res, {
              message: user.data
            },
            HttpStatus.BAD_REQUEST
          );

        }else{
          Response.success(res, {
            message: 'user updated',
            response: {
              email
            }
          });
        }

      }

    } catch (e) {
      this.handleError(e, res);
    }
  }

  async deleteUser(req, res){
    // TODO
    // This is to be used for test purposes only. A user should be deactivated and not deleted
    // delete by email

    // Auth ----------------------------------------------------------------------
    const endpoint_allowed_privileges = ['admin']
    const user_data = await AuthHelper.handle_auth(req, res, endpoint_allowed_privileges)
    // Auth ----------------------------------------------------------------------

    // const auth_email = user_data.email;
    // const auth_privilege = user_data.privilege;
    

    if (!req.query.email) {
      return Response.failure(
        res,
        {
          message:
            'Kindly add all field email to params'
        },
        HttpStatus.BAD_REQUEST
      );
    }

    const { email } = req.query;

    return this.userService
      .deleteUser(email)
      .then((result) => {
        this.logger.info('Deleted User successfully', result);
        if (result.errors){
          Response.failure(res, {
            message: result.errors,
            response: {},
          }, HttpStatus.BAD_REQUEST);
        }
        return Response.success(res,{
            message: 'User was successfully deleted',
            response: {email}
          },
          HttpStatus.OK
        );
      })
      .catch((deleteUserError) => {
        this.logger.error('Unable to delete User ======>', deleteUserError);
        return Response.failure(
          res,
          {
            message: createTicketError.msg,
            response: {}
          },
          HttpStatus.BAD_REQUEST
        );
      });


  }
}

module.exports = UserController;
