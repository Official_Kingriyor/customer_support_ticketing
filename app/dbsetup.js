const dotenv = require('dotenv');
dotenv.config();

// sample admin credentials
let adminEmail = "admin@gmail.com"
let adminPassword = "temp_pass"
let adminName = "admin"

// sample agent credentials
let agentEmail = "agent@gmail.com"
let agentPassword = "temp_pass"
let agentName = "agent"

// sample customer credentials
let customerEmail = "customer@gmail.com"
let customerPassword = "temp_pass"
let customerName = "customer"


const md5 = require('md5')
// service locator via dependency injection
const serviceLocator = require('./config/di');


// Connect to Mongo
serviceLocator.get('mongo');


class dbsetup {
    async execute(){
        const userService = serviceLocator.get('userService');
        const adminData = {
            name : adminName,
            email : adminEmail,
            password : md5(adminPassword),
            privilege: "admin",
            user_status : 'active'
        };

        const agentData = {
            name : agentName,
            email : agentEmail,
            password : md5(agentPassword),
            privilege: "agent",
            user_status : 'active'
        };

        const customerData = {
            name : customerName,
            email : customerEmail,
            password : md5(customerPassword),
            privilege: "customer",
            user_status : 'active'
        };
        
        try {
            const create_admin_res = await userService.createUser(adminData);
            if (create_admin_res.errmsg){
                console.log(create_admin_res.errmsg)
            }else{
                console.log('Admin has been created successfully')
            } 
        } catch (e) {
            console.log(e)
        }

        try {
            const create_agent_res = await userService.createUser(agentData);
            if (create_agent_res.errmsg){
                console.log(create_agent_res.errmsg)
            }else{
                console.log('Agent has been created successfully')
            } 
        } catch (e) {
            console.log(e)
        }

        try {
            const create_customer_res = await userService.createUser(customerData);
            if (create_customer_res.errmsg){
                console.log(create_customer_res.errmsg)
            }else{
                console.log('Customer has been created successfully')
            } 
        } catch (e) {
            console.log(e)
        }
        
        
    }
}

const script = new dbsetup()
script.execute()
