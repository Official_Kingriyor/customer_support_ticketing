/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/**
 * Created by Kingriyor
 */

const Response = require('../lib/response_manager');
const HttpStatus = require('../constants/http_status');
const config = require('../config/settings');
const auth_token = config.auth.token;
var jwt = require('jsonwebtoken');

class AuthHelper {

  static handleError(error, res) {
    if (error.name === 'DocumentNotFoundError') {
      return Response.failure(res, {
        message: error.message,
        response: {},
      }, HttpStatus.NOT_FOUND);
    } else {
      return Response.failure(res, {
        message: error,
        response: {},
      }, HttpStatus.BAD_REQUEST);
    }
  }

  static generateAccessToken(email, privilege) {
    // expires after an hour (3600 seconds = 60 minutes)
    const jwt_data = {
      email : email,
      privilege : privilege
    }
    return jwt.sign(jwt_data, auth_token, { expiresIn: 3600 });
  }

  static verifyToken(token){
    return jwt.verify(token, auth_token, function(err, decoded_data) {
      if (err){
        return {
            "error": true,
            "data": err
        }
      }
      // console.log(decoded_data)
      return {
          "error": false,
          "data": decoded_data
      }
    });
  }

  static async handle_auth(req, res, endpoint_allowed_privileges){
    if (!req.headers.authorization) {
      return this.handleError('No Auth credentials sent!', res);
    }

    const auth_tk = req.headers.authorization.replace("Bearer ","")

    let auth_res = this.verifyToken(auth_tk)
    if (auth_res.error){
      return this.handleError(auth_res.data, res);
    }
    if (endpoint_allowed_privileges.indexOf(auth_res.data.privilege) === -1){
      return this.handleError("User's privilege not permited here. Allowed users includes : " + endpoint_allowed_privileges, res);
    }
    return auth_res.data
  }
}

module.exports = AuthHelper;
