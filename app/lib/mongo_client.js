/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-destructuring */
const config = require('../config/settings');
const MongoClient = require('mongodb').MongoClient;

let client;
let db;

class _MongoClient {
  /**
     * Initialize mongo url
     */
  static init() {
    let connectionString;
    if (process.env.MONGO_CONNECTION_STRING) {
      connectionString = process.env.MONGO_CONNECTION_STRING;
    } else {
      connectionString = (!config.mongodb.username || !config.mongodb.password) ? `mongodb://${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.db}` : `mongodb://${config.mongodb.username}:${config.mongodb.password}@${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.db}`;
    }
    // Use connect method to connect to the server
    MongoClient.connect(connectionString, (err, _client) => {
      if (err) {
        console.error('could not connect to mongodb');
        console.error(err);
        process.exit(1);
      }
      console.log('Connected to mongodb');

      db = _client.db(config.mongodb.db);
      client = _client;
    });
  }

  static get client() {
    return client;
  }

  static get db() {
    return db;
  }
}

module.exports = _MongoClient;
