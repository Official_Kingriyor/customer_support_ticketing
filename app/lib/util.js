const json2csv_module = require('json2csv');

class util {

  

    static downloadResource = async (res, fileName, data) => {
      const fields = [
        {
          label: 'Ticket id',
          value: 'ticket_id'
        },
        {
          label: 'Customer email',
          value: 'customer_email'
        },
        {
          label: 'Estimated duration in days',
          value: 'estimated_duration_days'
        },
        {
         label: 'Ticket description',
          value: 'ticket_description'
        },
        {
         label: 'Agent email',
          value: 'agent_email'
        },
        {
         label: 'Request date',
          value: 'request_date'
        },
        {
         label: 'Process start date',
          value: 'process_start_date'
        },
        {
         label: 'Process end date',
          value: 'process_end_date'
        },
        {
         label: 'Ticket status',
          value: 'ticket_status'
        },
        // {
        //  label: 'Allow comments',
        //   value: 'allow_comments'
        // },
        // uncomment above to see commments in csv export
        {
         label: 'Comments',
          value: 'comments'
        }
      ];
      const json2csv = new json2csv_module.Parser({ fields });
      const csv = json2csv.parse(data);
      res.header('Content-Type', 'text/csv');
      res.attachment(fileName);
      return res.send(csv);
    }

}
module.exports = util;
