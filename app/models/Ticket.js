/* eslint-disable no-unused-vars */
/**
 * Created by Kingriyor
 */

const mongoose = require('mongoose');

const { Schema } = mongoose;
const config = require('../config/settings');

const collection = config.mongodb.collections;

const ticketSchema = new Schema({
  ticket_description: {
    type: String,
    required: true,
  },
  estimated_duration_days:{
    type: String
  },
  agent_email: {
    type: String
  },
  customer_email: {
    type: String,
    required: true,
  },
  ticket_id: {
    type: String,
    required: true,
  },
  request_date: {
    type: String,
    required: true,
  },
  process_start_date: {
    type: String,
    // required: true,
  },
  process_end_date: {
    type: String,
    // required: true,
  },
  ticket_status: {
    type: String,
    enum: ['pending', 'processing', 'blocked', 'closed'],
    required: true,
  },
  allow_comments: {
    type: Boolean,
    default: false
  },
  comments: [
    {
      user_type: {
        type: String
      },
      user_email: {
        type: String
      },
      message: {
        type: String
      },
      entry_time: {
        type: String
      }
      
    }
  ]
}, {
  timestamps: true,
  versionKey: false
});

const TicketModel = mongoose.model(collection.tickets, ticketSchema);

module.exports = TicketModel;
