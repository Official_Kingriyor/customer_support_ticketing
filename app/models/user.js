/**
 * Created by Kingriyor
 *
 */

const mongoose = require('mongoose');

const { Schema } = mongoose;
const config = require('../config/settings');

const collection = config.mongodb.collections;

const userSchema = new Schema({
  // user_id: {
  //   type: String,
  //   required: true,
  // },
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique : true
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  privilege: {
    type: String,
    enum: ['admin', 'agent', 'customer'],
    required: true,
  },
  user_status: {
    type: String,
    enum: ['active', 'inactive'],
    required: true,
  },
}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model(collection.user, userSchema);
