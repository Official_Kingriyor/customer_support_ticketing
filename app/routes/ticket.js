/**
 * Created by Kingriyor
 */

var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()
 
// create application/x-www-form-urlencoded parser
// var urlencodedParser = bodyParser.urlencoded({ extended: false })

const routes = function routes(server, serviceLocator) {
  const ticket = serviceLocator.get('ticketController');
  /**
     * Ticket Routes
  */

  server.get('/', (req, res) => res.send('Welcome to the Customer Support API'));

  server.get('/tickets', (req, res) => ticket.getTickets(req, res));

  server.get('/tickets/export', (req, res) => ticket.exportClosedTickets(req, res));

  server.get('/tickets/:ticket_id', (req, res) => ticket.getByTicketById(req, res));

  server.delete('/tickets/:ticket_id', (req, res) => ticket.deleteTicket(req, res));

  server.post('/tickets', jsonParser, (req, res) => ticket.createTicket(req, res));

  server.post('/tickets/comment', jsonParser, (req, res) => ticket.addComment(req, res));

  server.post('/tickets/process', jsonParser, (req, res) => ticket.processTicket(req, res));

  server.post('/tickets/close', jsonParser, (req, res) => ticket.closeTicket(req, res));

};


module.exports = routes;
