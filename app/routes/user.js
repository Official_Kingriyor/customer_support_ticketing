/**
 * Created by Kingriyor
 *
 */

var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()
 
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

const routes = function routes(server, serviceLocator) {
  const userController = serviceLocator.get('userController');
  /**
   * User Routes
  */

  server.get('/users', (req, res) => userController.getAllUsers(req, res));

  server.get('/users/email', (req, res) => userController.getUserByEmail(req, res));

  server.post('/users', jsonParser, (req, res) => userController.createUser(req, res));

  server.post('/users/register', jsonParser, (req, res) => userController.registerNewCustomer(req, res));

  server.put('/users/email', jsonParser, (req, res) => userController.updateUserByEmail(req, res));

  server.delete('/users/email', jsonParser, (req, res) => userController.deleteUser(req, res));

  server.post('/login', jsonParser, (req, res) => userController.login(req, res));

  server.get('/users/deactivate/email', (req, res) => userController.deactivareUser(req, res));

  server.get('/users/reactivate/email', (req, res) => userController.reactivareUser(req, res));

};


module.exports = routes;
