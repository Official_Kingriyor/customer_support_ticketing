/* eslint-disable no-console */
/* eslint-disable guard-for-in */
/* eslint-disable prefer-const */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint no-plusplus: ["error", { "allowForLoopAfterthoughts": true }] */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable camelcase */
/* eslint no-underscore-dangle: [2, { "allow": ["_id"] }] */
/**
 * Created by Kingriyor
*/

const MongoDBHelper = require('../lib/MongoDBHelper');
const AuthHelper = require('../lib/auth_helper');
const ticketModel = require('../models/Ticket');
// const DataPipelinePlugin = require('datapipeline-plugin');
const config = require('../config/settings');

// const dataPipelinePlugin = new DataPipelinePlugin({
//   source: "marketing-console"
// });

class Ticket {
  /**
     *
     * @param {*} logger Logger Object
     * @param mongoDBClient
     */
  constructor(logger, mongoDBClient) {
    this.logger = logger;
    this.mongoDBClientHelper = new MongoDBHelper(mongoDBClient, ticketModel);
  }


  /**
     * Gets a single Ticket
     *
     * @param ticket_id - Ticket ID to search for
     * @returns {Promise}
     */
  getTicketById(ticket_id) {
    return new Promise(async (resolve, reject) => {
      // const params = { ticket_id };
      const params = {}
      params.conditions = { ticket_id };
      // console.log(params)
      try {
        // const details = await this.mongoDBClientHelper.get(params);
        // const details = await this.mongoDBClientHelper.get(params);
        const details = await ticketModel.findOne(params.conditions);
        // console.log(details)
        return resolve(details);
      } catch (error) {
        return reject(error);
      }
    });
  }

  /**
   * Get All Tickets.
   *
   * @param params - Object with search parameters as key-value pair
   * @param user_privilege
   * @returns {Promise}
   */
  async getAllTickets(params) {

    // console.log(params)

    const searchParams = {};
    searchParams.conditions = {};

    if (params.size) {
      searchParams.limit = params.size;
    }
    // if (params.skip) {
    //   searchParams.skip = params.skip;
    // }
    
    // ensures a customer can only get their own records
    if (params.user_privilege === "customer"){
      searchParams.conditions.customer_email = params.customer_email;
    }

    if (params.user_privilege !== "customer"){
      if (params.customer_email){
        searchParams.conditions.customer_email = params.customer_email;
      }
    }

    if (params.allow_comments){
      searchParams.conditions.allow_comments = params.allow_comments;
    }

    if (params.ticket_id){
      searchParams.conditions.ticket_id = params.ticket_id;
    }

    if (params.ticket_status){
      searchParams.conditions.ticket_status = params.ticket_status;
    }

    // createdAt date filter --------------------------------
    if (params.start_date && params.end_date) {
      let start_date = parseInt(params.start_date, 10);
      let end_date = parseInt(params.end_date, 10);

      start_date = new Date(start_date);
      end_date = new Date(end_date);
      searchParams.conditions.createdAt = {
          $lte: end_date,
          $gte: start_date
        }
    }
    // createdAt date filter --------------------------------

    // filter process_end_date
    if (params.past_month && params.current_epoch) {
      searchParams.conditions.process_end_date = {
          $lte: params.current_epoch,
          $gte: params.past_month
        }
    }
    
    console.log(searchParams)

    return this.mongoDBClientHelper.getall(searchParams);
  }


  /**
     * Saves the Ticket data into MongoDB or
     * 
     *
     * @param ticketData - Object containing requests parameters and values
     * @returns {Promise}
     */
  async createTicket(ticketData) {
    // set the ticket_status to pending since the ticket is new ...
    ticketData.ticket_status = config.ticket_status.pending ;
    try {
      const savedData = await ticketModel.create(ticketData);
      return savedData.toObject();
    } catch (err) {
      return err;
    }
  }

  async deleteTicket(ticket_id){
    try {
      // const details = await this.mongoDBClientHelper.get({ ticket_id });
      const details = await ticketModel.findOne({ ticket_id });


      if (!details){
        return {
          error : true,
          data: "Ticket not found"
        }
      }

      const deleteData = await ticketModel.deleteOne({ ticket_id });
      // let result = deleteData//.toObject();
      let result = JSON.stringify(deleteData)
      result = JSON.parse(result);
      return result
    } catch (err) {
      return err;
    }
  }

  /**
     * Adds comment to a ticket
     * 
     * @param user_privilege
     * @param ticket_id
     * @param ticketData - Object containing requests parameters and values
     * @returns {Promise}
     */
  async addComment(user_privilege, ticket_id, ticketData) {
    
    // ticketData should have user_id, and comment
    const comment_object = {
      "user_type" : user_privilege,
      "user_email" : ticketData.user_email,
      "message" : ticketData.comment,
      "entry_time" : Math.floor(Date.now() / 1000)
    }

    let updateData = {}
    
    const params = {}
    params.conditions = { ticket_id };
    // console.log(params)
    try {
      // const details = await this.mongoDBClientHelper.get(params);
      const details = await ticketModel.findOne({ ticket_id });

      if (!details){
        return {
          error: true,
          data: "Ticket not found"
        }
      }

      if (user_privilege === "customer" && details.allow_comments !== true){
        // ensure allow_comments == true
        return {
          error: true,
          data: "Customer can't comment first"
        }
      }

      if (user_privilege === "customer" && details.ticket_status === config.ticket_status.closed){
        // ensure ticket isn't closed
        return {
          error: true,
          data: "Customer can't comment on a closed ticket"
        }
      }

      const mongo_id = details._id 
      updateData._id = mongo_id
      try {
        const savedData = await ticketModel
        .findOneAndUpdate(
          { _id: mongo_id },
          { $addToSet: { comments: comment_object }, "allow_comments" : true }
        )
        return {
          error: false,
          data: savedData.toObject()
        }
      } catch (err) {
        return {
          error: true,
          data: err
        }
      }
    } catch (error) {
      return {
        error: true,
        data: error
      }
    }

    
  }

  /**
     * Update a ticket
     * 
     * @param user_privilege
     * @param ticket_id
     * @param ticketData - Object containing requests parameters and values
     * @returns {Promise}
     */
  async updateTicket(user_privilege, ticket_id, ticketData) {

    let updateData = {};
    if (ticketData.process_start_date){
      updateData.process_start_date = ticketData.process_start_date
    }
    if (ticketData.process_end_date){
      updateData.process_end_date = ticketData.process_end_date
    }
    if (ticketData.agent_email){
      updateData.agent_email = ticketData.agent_email
    }
    if (ticketData.ticket_status){
      updateData.ticket_status = ticketData.ticket_status
    }
    if (ticketData.estimated_duration_days){
      updateData.estimated_duration_days = ticketData.estimated_duration_days
    }

    const params = {}
    params.conditions = { ticket_id };
    // console.log(params)
    try {
      // const details = await this.mongoDBClientHelper.get(params);
      const details = await ticketModel.findOne(params.conditions);
      // console.log(details)

      if (!details){
        return {
          error : true,
          data: "Ticket not found"
        }
      }

      const mongo_id = details._id 
      // updateData._id = mongo_id
      try {
        const savedData = await ticketModel
        .findOneAndUpdate(
          { _id: mongo_id },
          updateData
        )
        return {
          error : false,
          data: savedData.toObject()
        }
      } catch (err) {
        return {
          error : true,
          data: err
        }
      }
    } catch (error) {
      return {
        error : true,
        data: error
      }
    }
    
  }

}

module.exports = Ticket;
