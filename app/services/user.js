/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/**
 * Created by Kingriyor
 *
*/

const MongoDBHelper = require('../lib/MongoDBHelper');
const userModel = require('../models/user');
const MongoClient = require('../lib/mongo_client');
const ObjectId = require('mongodb').ObjectID;

MongoClient.init();

class UserService {
  /**
   *
   * @param {*} logger Logger Object
   * @param mongoDBClient
   */
  constructor(logger, mongoDBClient) {
    this.logger = logger;
    this.mongoDBClientHelper = new MongoDBHelper(mongoDBClient, userModel);
  }

  /**
   * Gets user by email
   *
   * @param email (email)
   * @returns user object
   */
  getUserByEmail(email) {
    return new Promise((resolve, reject) => {
      const params = {};
      params.conditions = { email: email };
      // return ticketModel.findOne({ email: email })
      return this.mongoDBClientHelper.get(params)
        .then(_details => resolve(_details))
        .catch(error => reject(error));
    });
  }

  /**
   * Gets user by email and password
   *
   * @param email (email)
   * @param password (password)
   * @returns user object
   */
  getUserByEmailAndPassword(email, password) {
    return new Promise((resolve, reject) => {
      const params = {};
      params.conditions = { email: email, password: password };
      return this.mongoDBClientHelper.get(params)
        .then(_details => resolve(_details))
        .catch(error => reject(error));
    });
  }

  /**
   * Get All Users.
   *
   * @param params - Object with search parameters as key-value pair
   * 
   * @returns {Promise}
   */
  async getAllUsers(params) {
    const searchParams = {};
    searchParams.conditions = {};

    if (params.size) {
      searchParams.limit = params.size;
    }
    if (params.email) {
      searchParams.conditions.email = params.email;
    }

    // createdAt date filter --------------------------------
    if (params.start_date && params.end_date) {
      let start_date = parseInt(params.start_date, 10);
      let end_date = parseInt(params.end_date, 10);

      start_date = new Date(start_date);
      end_date = new Date(end_date);
      searchParams.conditions.createdAt = {
          $lte: end_date,
          $gte: start_date
        }
    }
    // createdAt date filter --------------------------------

    console.log(searchParams)
    
    return this.mongoDBClientHelper.getBulk(searchParams);
  }

  /**
   * Saves the user data into MongoDB or
   * updates the record if it already exists.
   *
   * @param userData - Object containing requests parameters and values
   * @returns {Promise}
   */
  async createUser(userData) {
    return userModel.create(userData)
      .then(savedData => savedData.toObject())
      .catch(err => err);
  }

  /**
   * Gets User data by id from MongoDB
   *
   * @param userData - Object containing requests parameters and values
   * @returns {Promise}
   */
  async getUserById(userData) {
    const { _id } = userData;
    return new Promise((resolve, reject) => {
      const data = MongoClient.db.collection('user').aggregate([
        {
          $match: { _id: ObjectId(_id) }
        }]);

      data.each((err, doc) => {
        if (err) {
          reject(err);
        }

        if (doc) {
          resolve(doc);
        }
      });
    });
  }

  /**
   * Updates the user data in MongoDB
   *
   * @param userData - Object containing requests parameters and values
   * @returns {Promise}
   */
  async updateUserName(email, name) {
    try{
      const updateData = await userModel.findOneAndUpdate(
        { email: email },
        { name },
        { new: true },
      );
      return {
        error : false,
        data: updateData.toObject()
      }

    } catch (err){
      return {
        error : true,
        data: err
      }
    }
    
  }

  /**
   * Updates the user status in MongoDB
   *
   * @param userData - Object containing requests parameters and values
   * @returns {Promise}
   */
  async updateUserStatus(userData) {
    const { email, user_status } = userData;

    return await userModel.findOneAndUpdate(
      { email },
      { user_status },
      { new: true },
    );
  }

  async deleteUser(email){
    try {
      const deleteData = await userModel.deleteOne({ email });
      return deleteData.toObject();
    } catch (err) {
      return err;
    }
  }

}

module.exports = UserService;
