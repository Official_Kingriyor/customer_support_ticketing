const app = require('../app/api');

const chai = require("chai");
const chaiHttp = require("chai-http");
let adminEmail = "admin@gmail.com"
let adminPassword = "temp_pass"
let adminToken = ""
let agentEmail = "test_agent@gmail.com"
let agentPassword = "temp_pass"
let agentToken = ""
let customerEmail = "test_customer@gmail.com"
let customerPassword = "temp_pass"
let customerToken = ""
let ticket_id = ""


const { expect } = chai;
chai.use(chaiHttp);

describe("Server!", () => {

  it("welcomes user to the api", done => {
    chai
      .request(app)
      .get("/")
      .end((err, res) => {
        console.log(res.text)
        // expect(res).to.have.status(200);
        expect(res.statusCode).to.equals(200);
        expect(res.text).to.equals("Welcome to the Customer Support API");
        done();
      });
  });

  it("admin login", done => {
    chai
      .request(app)
      .post("/login")
      .send({ email: adminEmail, password: adminPassword })
      .end((err, res) => {
        // console.log(res.body);
        adminToken = res.body.data.token;
        // console.log(adminToken);
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("create agent", done => {
    try{
      chai
        .request(app)
        .post("/users")
        .set("Authorization",  `Bearer ${adminToken}`)
        .send({
          "name":"test agent",
          "email": agentEmail,
          "password": agentPassword,
          "privilege":"agent"
        })
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("agent login", done => {
    chai
      .request(app)
      .post("/login")
      .send({ email: agentEmail, password: agentPassword })
      .end((err, res) => {
        // console.log(res.body);
        agentToken = res.body.data.token;
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("register new customer", done => {
    try{
      chai
        .request(app)
        .post("/users/register")
        // .set("Authorization",  `Bearer ${adminToken}`)
        .send({
          "name":"test agent",
          "email": customerEmail,
          "password": customerPassword
        })
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("customer login", done => {
    chai
      .request(app)
      .post("/login")
      .send({ email: customerEmail, password: customerPassword })
      .end((err, res) => {
        // console.log(res.body);
        customerToken = res.body.data.token;
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("getall users", done => {
    chai
      .request(app)
      .get("/users")
      .set("Authorization",  `Bearer ${adminToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("update user info", done => {
    chai
      .request(app)
      .put("/users/email")
      .set("Authorization",  `Bearer ${adminToken}`)
      .send({
        "name":"updated customer name",
        "email": customerEmail,
      })
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("get user via email", done => {
    chai
      .request(app)
      .get("/users/email?email=" + customerEmail)
      .set("Authorization",  `Bearer ${agentToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("deactivate user via email", done => {
    chai
      .request(app)
      .get("/users/deactivate/email?email=" + customerEmail)
      .set("Authorization",  `Bearer ${adminToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("reactivate user via email", done => {
    chai
      .request(app)
      .get("/users/reactivate/email?email=" + customerEmail)
      .set("Authorization",  `Bearer ${adminToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("open ticket", done => {
    try{
      chai
        .request(app)
        .post("/tickets")
        .set("Authorization",  `Bearer ${customerToken}`)
        .send({
          "ticket_description" : "unittesting time"
        })
        .end((err, res) => {
          // console.log(res.body);
          ticket_id = res.body.data.ticket_id
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("getall tickets", done => {
    chai
      .request(app)
      .get("/tickets")
      .set("Authorization",  `Bearer ${adminToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("get ticket via ticket_id", done => {
    chai
      .request(app)
      .get("/tickets/" + ticket_id)
      .set("Authorization",  `Bearer ${agentToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body.error).to.equals(false);
        done();
      });
  });

  it("process ticket", done => {
    try{
      chai
        .request(app)
        .post("/tickets/process")
        .set("Authorization", `Bearer ${agentToken}`)
        .send({
          "ticket_id" : ticket_id,
	        "estimated_duration_days" : 5
        })
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("close ticket", done => {
    try{
      chai
        .request(app)
        .post("/tickets/close")
        .set("Authorization", `Bearer ${agentToken}`)
        .send({
          "ticket_id" : ticket_id
        })
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("export csv", done => {
    chai
      .request(app)
      .get("/tickets/export")
      .set("Authorization",  `Bearer ${adminToken}`)
      .end((err, res) => {
        // console.log(res.body)
        expect(res).to.have.status(200);
        // expect(res.body.error).to.equals(false);
        done();
      });
  });


  // DELETE CREATED RESOURCES

  it("delete ticket", done => {
    try{
      chai
        .request(app)
        .delete("/tickets/" + ticket_id)
        .set("Authorization",  `Bearer ${adminToken}`)
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("delete agent", done => {
    try{
      chai
        .request(app)
        .delete("/users/email?email=" + agentEmail)
        .set("Authorization",  `Bearer ${adminToken}`)
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  it("delete customer", done => {
    try{
      chai
        .request(app)
        .delete("/users/email?email=" + customerEmail)
        .set("Authorization",  `Bearer ${adminToken}`)
        .end((err, res) => {
          // console.log(res.body);
          expect(res).to.have.status(200);
          expect(res.body.error).to.equals(false);
          done();
        });
    } catch(err){
      console.log(err)
    }
  });

  
});